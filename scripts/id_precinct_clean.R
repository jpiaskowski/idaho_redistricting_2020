
library(here); library(dplyr); library(tidyr); library(stringr)
library(rgdal)

# precinct rename file (created manually from inspecting the raw data)
rename_file <- read.csv(here("outputs", "precinct_rename_file.csv")) %>% 
  distinct(right, wrong)

# census voting districts
id_votdist <- read.csv(here("outputs", "id2020_pct_summary.csv")) %>%
  select(1:4) %>% mutate(name = tolower(str_remove(name, " County$"))) %>% 
  arrange(county, precinct) %>% mutate(id_votdist = 1)

counties <- id_votdist %>% distinct(name, county)

# 2018 precinct-level boundaries 
pct_sh <- readOGR(dsn = here("data", "shapefiles"), 
                    layer = "id_2018") 
pct_sh_df <- pct_sh@data %>% arrange(VTDST) %>% 
  select(precinct_sh = NAME, vtd = VTDST) %>% 
  mutate(pct_sh = 1)  
#rm(pct_sh)

combine_vtd <- full_join(id_votdist, pct_sh_df, by = "vtd") %>% 
  rowwise() %>% mutate(both = sum(id_votdist, pct_sh, na.rm = TRUE)) %>% 
  mutate(county2 = as.character(county)) %>% 
  mutate(county3 = case_when(
    is.na(county) ~ str_sub(vtd,1,nchar(vtd)-4),
    TRUE ~ county2)) %>% #filter(is.na(county)) %>% 
  mutate(county = as.integer(county3)) %>% select(-county2, -county3, -name) %>% 
  full_join(counties, by = "county") %>% arrange(vtd) %>% 
  select(name, county, vtd, precinct, precinct_sh, id_votdist, pct_sh, both) %>% 
  rename(precinct_census = "precinct")

apply(combine_vtd, 2, function(x) sum(is.na(x)))

write.csv(combine_vtd, here("outputs", "temp_precinct_file_census.csv"), row.names = FALSE)

comb_miss <- filter(combine_vtd, both == 1)

comb_pct_name <- combine_vtd %>% select(-id_votdist, -pct_sh, -both) %>% 
  mutate(precinct_census2 = tolower(precinct_census), 
         precinct_sh2 = tolower(precinct_sh))

comb_pct_name$precinct_census2 <- gsub(" |#", "", comb_pct_name$precinct_census2)
comb_pct_name$precinct_sh2 <- gsub(" |#", "", comb_pct_name$precinct_sh2)

comb_pct_name$match <- comb_pct_name$precinct_census2 == comb_pct_name$precinct_sh2
table(comb_pct_name$match)

# idaho election results precinct info
election_stats <- read.csv(here("outputs", "gen_stats_leg_pct.csv")) %>% 
  distinct(precinct, district, county) %>% mutate(election_stats = 1) %>% 
  arrange(county, precinct) %>% mutate(county = tolower(county))
election_results <- read.csv(here("outputs", "voting_records_2016-2020.csv")) %>% 
  mutate(county = gsub(" (continued)", "", county, fixed = TRUE)) %>% 
  distinct(county, precinct) %>% mutate(election_results = 1) %>% 
  arrange(county, precinct) %>% mutate(county = tolower(county)) 

# test
# grep("bonner (continued)", election_results$county, fixed = TRUE) == 0
# length(unique(election_results$county)) == 44

id_elect <- full_join(election_stats, election_results, by = c("county", "precinct")) %>% 
  rename(county_name = "county") %>% 
  left_join(counties, by = c("county_name" = "name")) %>% 
  select(district, county, county_name, precinct, election_stats, election_results) %>% 
  rowwise() %>% mutate(both = sum(election_stats, election_results, na.rm = TRUE)) 
  arrange(district, county, precinct) 

write.csv(id_elect, here("outputs", "temp_precinct_file_idaho_elect.csv"), row.names = FALSE)

name_replacement <- function(vec) {
  new_names <- vapply(vec, function(x) {
    test1 = rename_file$wrong %in% x
    if(any(test1)){ 
      if(sum(test1) > 1) 
        "multiple matches"
      else
        rename_file$right[which(test1)]
    } else x
  }, FUN.VALUE = "character", USE.NAMES = F)
  return(new_names)
}

id_elect_name <- select(id_elect, -election_stats, -election_results, -both) %>% 
  mutate(precinct2 =  name_replacement(precinct)) %>% 
  mutate(precinct2 = tolower(precinct2)) %>% 
  mutate(precinct2 = gsub(" |#", "", precinct2)) %>% 
  rename(precinct_raw = "precinct") %>% rename(precinct = "precinct2") %>% 
  rename(LD = "district") %>% 
  mutate(district = str_extract_all(LD, "[0-9]+", simplify = TRUE), 
         county = as.character(county))

# filter(id_elect_name, precinct == "multiple matches") # should be zero

vtd_names_long <- comb_pct_name %>% select(vtd, precinct_census2, precinct_sh2) %>% 
  pivot_longer(cols = 2:3, names_to = "file", values_to = "precinct") %>% 
  distinct(vtd, precinct) %>% drop_na()
  
precinct_fullmatch <- vtd_names_long %>% 
  mutate(county = substr(vtd, 1, 2), district = substr(vtd, 3, 4)) %>% 
  inner_join(id_elect_name, by = c("county", "district", "precinct")) %>%
  arrange(vtd)



