
## Data Sources

#### Election Results
* Voting results for 2016, 2018 and 20202 downloaded from the [Idaho Secretary of State website](https://sos.idaho.gov/elections-division/election-results/).
  * **gen_leg_pct** provides general election results for every precinct for every Idaho State Legislative Position (House and Senate). 
  * **gen_stats_leg_pct** contains data on total number of registered voters, percent who voted, day of registrants, ballots cast, etc for every precinct in Idaho. 
  * **gen_stwd_pct** contains info from every precinct in Idaho on voting stats and the outcomes of statewide measures such as Supreme Court elections and constitutional amendments. 
  * **_cnty** provides stats at the county level
  * **gen_dstjudg_pct** contains precinct-level data on district judges general election results
  * **pri_** refers to primary results

#### Precinct Shapefiles   

* Precinct shapefiles
  * [2018 data](https://dataverse.harvard.edu/api/access/datafile/:persistentId?persistentId=doi:10.7910/DVN/UBKYRU/DFEQHK) from Harvard dataverse
  * [2016 data](https://dataverse.harvard.edu/api/access/datafile/:persistentId?persistentId=doi:10.7910/DVN/NH5S2I/XBOK2Q) from Harvard dataverse
  * [Census shapefiles for Idaho](https://www2.census.gov/geo/tiger/TIGER2020PL/STATE/16_IDAHO/) (*note:* these are big, so I decided not to load them into the git repo)

#### 2020 Census

  * [2020 Census (P.L. 94-171) Redistricting Data Summary Files](https://www.census.gov/programs-surveys/decennial-census/about/rdo/summary-files.html)
  * [File](https://www2.census.gov/programs-surveys/decennial/2020/technical-documentation/complete-tech-docs/summary-file/2020Census_PL94_171Redistricting_StatesTechDoc_English.pdf) summarising what the column headers in those files mean. Info on tables P1 (from "part1") begin on page 25. Info on summary levels begins on page 65. 
  * [codes](https://www2.census.gov/programs-surveys/decennial/rdo/about/2020-census-program/Phase3/SupportMaterials/FrequentSummaryLevels.pdf) for different levels of data summary

## R Resources

* [r/tigris](https://cran.r-project.org/web/packages/tigris/tigris.pdf) package for shape files
* [r/idbr](https://cran.r-project.org/web/packages/idbr/index.html) package to access the Census API
* [tidycensus](https://cran.r-project.org/web/packages/tidycensus/index.html) working with census data
* [acs](https://cran.r-project.org/web/packages/acs/index.html) package for working the American Community Survey and other census data
* [more resources](https://rconsortium.github.io/censusguide/r-packages-all.html) provided by the R consortium

## Other Resources

* [Brennan Project on redistricting in Idaho](https://www.brennancenter.org/sites/default/files/publications/2019_06_50States_FINALsinglepages_12.pdf)
* [tigris package](https://www.rdocumentation.org/packages/tigris/versions/1.0)
* [OpenPrecincts](https://openprecincts.org/id/)  (they have nothing for Idaho)
* [Princeton Gerrymander Project](https://gerrymander.princeton.edu/reforms/ID)
* [DRA Project](https://davesredistricting.org) interactive maps, etc
